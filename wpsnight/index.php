<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="de"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="de"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="de"> <![endif]-->
<!--[if gt IE 8]><html class="ie ie9" lang="de"> <![endif]-->
<!--[if IE]><html class="ie" lang="de"> <![endif]-->

<!--[if !IE]><!--><html lang="de"> <!--<![endif]-->
<head>
	<!-- Todoos
	==================================================
    - Google-Analytics einbinden
    - PHP-Files
        - sessions
        - cockies
        - connect-script
        - file upload
        - mail script ( mit initialisieren eines Mail accounts)
    

    -->

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>wps night 2016</title>
	<meta name="description" content="">
    <meta name="keywords" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="Muster Page">
    
    <meta name='application-name' content='wps night 2016' />
    
	<!-- PHP
	================================================== -->
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="_css/needs.css">
	<link rel="stylesheet" href="_css/layout.css">
	<link rel="stylesheet" href="_css/html5_animations.css">
    
	<!-- JavaScript
	================================================== -->
	<!--[if !IE]><!-->
	<script>
		if (/*@cc_on!@*/false) {
			document.documentElement.className+=' ie ie10';
		}
	</script><!--<![endif]-->

	<script src="_script/js/lib/jquery-1.11.0.min.js"></script>
	<script src="_script/js/lib/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<script src="_script/js/lib/placeholder.js"></script>
    
   	<script src="_script/js/needs.js"></script>
    <script src="_script/js/theme.js"></script>
    
	<!-- Favicons
	================================================== -->
	<link href="_img/favicon.ico" rel="shortcut icon">
    

</head>
<body class="need-js with-sticky-footer">
        
	<div class="body-wrapper">
	
          
    <!-- Page Wrapping
    ================================================== -->
    <div class="page-wrapper">
    
        <!-- Primary Page Layout
        ================================================== -->

        <div class="container">
        	<div class="page-header"><img src="_img/header.jpg" alt="wps night"/><div class="logo"><img src="_img/wps_green.png" alt="wps logo"/></div>
            </div>
            <div class="row">
                <div class="col-8 col-push-2">
                    <h1>wps night</h1>
                    <p>wps lädt Sie herzlich zur ersten Ausgabe der wps night ein. Leckeres Essen, tolle Leute, interessante Gespräche, gute Musik und spannende Einblicke in andere Welten.</p>
                    <div class="small info">
                    
                    <hr>
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td>Wann</td>
                              <td>Donnerstag, 8. September 2016 ab 17 Uhr</td>
                            </tr>
                            <tr>
                              <td>Wo</td>
                              <td>Heimstrasse 46, 8953 Dietikon</td>
                            </tr>
                            <tr>
                              <td>Anreise mit ÖV</td>
                              <td>Ab Bahnhof Dietikon mit der Buslinie 309 Richtung Silbern. Bei Haltestelle Pestalozzi aussteigen</td>
                            </tr>
                            <tr>
                              <td class="no-border">Anreise mit dem Auto</td>
                              <td class="no-border">Es stehen genügend Parkplätze zur Verfügung. Die Parkmöglichkeiten 
                        sind ausgeschildert, und hilfsbereite wps Mitarbeitende helfen Ihnen vor Ort gerne weiter.</td>
                            </tr>
                          </tbody>
                        </table>
                        
                    <hr>
                    </div>
                    <p style="margin-bottom:30px;">Liebe Gäste, die wps night findet sowohl drinnen wie draussen statt. Bei spätsommerlichen Nächten ist etwas Warmes zum Anziehen nie Fehl am Platz.</p>
    
                </div>
            </div>
            
            <div class="row add-bottom">
            	<div class="col-4 col-push-2"><img src="_img/plan_big.png" alt="plan big"/>
                </div>
                <div class="col-4"><img src="_img/plan_small.png" alt="plan small"/></div>
            </div>
            
            
            
            <div class="row add-bottom">
            	<div class="col-8 col-push-2">
                    <hr>
                    <p style="margin-top:30px;"><a href="_downloads/wps_Einladung.pdf" target="new" class="smooth5">Laden Sie hier das PDF</a> mit sämtlichen Informationen herunter.                </p>
            	</div>
            </div>
        </div>
        
        
        
        
            
	
    
    <!-- End Page Body (page-wrapper)
    ================================================== -->
    </div>
        
        

    
<!-- Document end
================================================== -->
</div>
</body>
</html>