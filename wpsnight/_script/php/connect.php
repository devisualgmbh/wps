<?php


// Database settings 
	$mysqlsettings = [
			'host' => 'localhost',
			'user' => 'root',
			'pass' => 'root',
			'dbname' => 'muster'
		];

// connection with PDO
	try {
		
		$conn = new PDO('mysql:host='.$mysqlsettings["host"] .';dbname='. $mysqlsettings["dbname"], $mysqlsettings["user"], $mysqlsettings["pass"]);
		
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
	}
	
	
	
	
	
// example
	$data = $conn->query('SELECT * FROM options');
	
    foreach($data as $key => $value ) {
		print_r($value);
    }
	
	
    while($user = $data->fetch()) {
        print_r($user); 
    }

?>