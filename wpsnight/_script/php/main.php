<?php
	
	/* #PHP Settings
	================================================== */
	
?>

<?php


function nav($kind, $id, $class) {
	
		$navigation = [
			array(),
	  	];
	 
	 
	
	// switch navigations and settings
	switch ($kind) {
		case 'mobile' :
			$mobile =  true;
			$classes = 'nav-mobile shifter-navigation '.$class;
			break;
		case 'main' :
		case 'nav-main' :
		case 'main-nav' :
			$classes = 'nav-main '.$class;
			break;
	}
	
	$return = '<nav id="'. $id .'" class="nav clearfix '.$classes.'">';
	if ($mobile) {
		$return .= '<div class="mobilenavcontrol nav-mobile-header"><p>Navigation</p></div>';
	}
	
	
	// output topnavi
	$topnavi_i = 0;
	$return .= '<ul class="topnav clearfix">';
	foreach ($navigation as $topnavilink => $topnavi) {
		$return .= '<li class="'. ($topnavi['sub'] ? 'with-child' : false) .' list-'. $topnavi["classes"] .'">';
		$return .= '<a class="'. $topnavi["classes"] .'" href="'. $topnavi["link"] .'" target="'. $topnavi["target"] .'">'. ($topnavi["name-out"]) .'</a>';
		
		// output subnavi	
		if ($topnavi["sub"]) {
			$subnavi_i = 0;
			$return .= '<ul class="subnav">';
			foreach ($topnavi["sub"] as $key => $subnavi) {
				$return .= '<li class="list-'. $subnavi["classes"] .'">';
				$return .= '<a class="'. $subnavi["classes"] .'" href="'. $subnavi["link"] .'" target="'. $subnavi["target"] .'">'. ($subnavi["name-out"]) .'</a></li>';
			}
			$return .= '</ul>';
		} 
		$return .= '</li>';
		
	}
	$return .= '</ul>
      </nav>
';
	echo $return;
}

?>