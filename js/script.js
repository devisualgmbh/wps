
var bSupportsHistory = !!(window.history && history.pushState);

// pages
var pages = ["communication", "picture", "solution", "about"];
// diese funktion wird mit true und false abgefüllt, wenn es eine übergang gibt 
// zwischen zwei pages und er scrollTop() drin ist, damit der .scroll listener nicht aktiviert wird.
var isScroll = false;

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}
 
function removeMenuActive() {
    for (var i=0; i<pages.length; i++){ 
		$('#desktopMenu').removeClass(pages[i]); 
		$('#ajaxContentDesktop').removeClass(pages[i]); 
	}
}

function loadAjaxContent(page, name) {
	$(page)
		.load("assets/"+name+".html")
		.addClass("active")
		.show('slide', { direction: "rigth" }, 500);
}

function contactScroll(name){
	var activeScroll = false;
	var ajaxContent = $('#ajaxContentDesktop');
	 
	$( window ).unbind();
	ajaxContent.unbind();
	
	if( ajaxContent.is(".active") ){
		
		ajaxContent.unbind();
		
		ajaxContent.scroll(function(){
		
			if(activeScroll == false && isScroll==false){

			var math= $("#unitSection").height()-$("#kontakt").outerHeight(true) - $("#footer").outerHeight(true);

		
				if(  math  <= ajaxContent.scrollTop()){		
						
					var header= document.getElementById("header-title").innerHTML;
					var headerText= 'Kontakt <img src="css/icons/icon_'+name+'_darkgray.svg" width="33px" height="33px" class="title-icon hidden-xs">';
					
					if(header != headerText){
						activeScroll = true;
				
						$("#header-title").fadeOut(200);
						
						setTimeout(function(){
							$("#header").addClass("bg-grey");
							document.getElementById("header-title").innerHTML = headerText;
							$("#header-title").fadeIn(200);
							setTimeout(function(){ activeScroll = false; }, 200);
						}, 200);
						
						$("#contact-span").fadeOut(200);
					}
					
					
					
				}
				else{
					var header= document.getElementById("header-title").innerHTML;
					
					var headerText= toTitleCase(name)+' <img src="css/icons/icon_'+name+'_darkgray.svg" width="33px" height="33px" class="title-icon hidden-xs">';
					
					
					if(header != headerText){
						
						activeScroll = true;
						
						$("#header-title").fadeOut(200);
						setTimeout(function(){
							document.getElementById("header-title").innerHTML = headerText;
							$("#header").removeClass("bg-grey");
							$("#header-title").fadeIn(200);
							$("#contact-span").fadeIn(200);
							setTimeout(function(){ activeScroll = false; }, 200);
						}, 200);
						
						
					}
					
					
				}
			}
		});
	}
	
	$( window ).resize(function() {
    	contactScroll(name);
    });
}


function logoBackToHome(className) {
       $("." + className).click(function () {
            window.location.href = "/";
      });
}

function logoBackToHomeDesktop(className) {
	$("." + className).click(function () {
		$('.desktop.menuButton').removeClass("open");
		$('.desktop-menu').toggle('slide', { direction: "right" }, 500);
		
		if($('.ajaxContent').is('.active')){
			$('.ajaxContent').hide('slide', { direction: "right" }, 500).removeClass('active');
		}
		if($('.ajaxUnitcontent').is('.active')){
			$('.ajaxUnitcontent').hide('slide', { direction: "right" }, 500).removeClass('active');
		}
		
		$(".ajaxContent").removeClass('active');  
		$(".logo").removeClass('hidden-xs');
		$(".desktop.menuButton").removeClass('thirdlevel').removeClass('third');
		$(".backBottommenu").removeClass("active");
		$('.unitMenu').show();
		$('.contact-base').removeClass("active");  
	});
} 


function contactEvents(name) {
 
	$("#contact-" + name).click(function () {
	
		localStorage.setItem("thirdLevelPage", name);
		
		
		$(this).toggleClass('active');
		
		$(".ajaxContentContact").toggle('slide', { direction: "rigth" }, 300)
		                 .addClass('active')
		                 .load("assets/contactform.html");
		$('.backTopmenu').addClass('active');
		$('.backBottommenu').addClass('active');
		$('.menuButton').toggleClass('fourth');
		
		setTimeout(function() {
			logoBackToHome('wps-logo-to-home');
    	}, 100);
	});
	
}

function hideImpressum(slide) {
	if (slide) {
		$(".ajaxUnitcontent")
			.hide('slide', { direction: "rigth" }, 500)
			.removeClass('active');
	}
	else{
	
		$(".ajaxUnitcontent")
			.hide(0)
			.removeClass('active');
	}
}

function impressumEvent() {
	$(".impressum").click(function () {
		var name = "impressum";
		PushState("reloadPage", "#" + name, "#" + name);
		removeMenuActive();
		$(".ajaxUnitcontent")
			.show('slide', { direction: "rigth" }, 300)
			.addClass('active')
			.load("assets/"+name+".html");
		
		setTimeout(function() {
			
			contactScroll(name);				
			contactEvents(name);
			setTimeout(function() {
			
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2Desktop(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}

			}, 100);
		}, 100);
		
	});
}

function contactEventsDesktop(name) {
	$("#contact-" + name).click(function () {
		$(this).toggleClass('active');
		$('.unitMenu').toggle();
		$(".ajaxContentContact").toggle('slide', { direction: "rigth" }, 300)
		                 .toggleClass('active')
		                 .load("assets/contactform.html");
		$('.desktop-backTopmenu')
			.addClass('active')
			.css("display","block");

		$('.desktop.menuButton').toggleClass('fourth');
		
		setTimeout(function() {
			logoBackToHomeDesktop('wps-logo-to-home');
		
    	}, 100);
	});
}

function pageEvents(name) {
	$("." + name).click(function () {
		removeMenuActive();
		PushState("reloadPage", "#" + name, "#" + name);
		$("#mobile-map").removeClass("active");
		$("#topNav-mobile").removeClass("active");
		$(".logo").addClass("hidden-xs");
		$(".logo-dark").addClass("hidden-xs"); 
		$("#mobileMenu").addClass("thirdlevel-menu");
		
		hideImpressum(false);
		
		isScroll=true;
		
		$(".ajaxContent").show(0).addClass('active').css("overflow","scroll").css("-webkit-overflow-scrolling" ,"touch").load("assets/"+name+".html").scrollTop(0);
		
		setTimeout(function () {
			isScroll=false;
		}, 300);
		
			
		$('.mobile.menuButton').addClass('thirdlevel-menu').removeClass("open");
		
		
			$('#mobileMenu').hide('slide', { direction: "down" },500);
			//$('#ajaxContentMobile').scrollTop(0);
		
		
		
		
		$('#mobileMenu').addClass(''+name+''); 
		$('.ajaxContent').addClass(''+name+'');
		
	
		
		
		
		setTimeout(function() {
			impressumEvent();
			contactScroll(name);				
			
			contactEvents(name);
			
			
			
			setTimeout(function() {
				
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
				
			}, 100);
		}, 100);
		
		
	});
}

function pageEventsDesktop(name){
	$(".desktop-"+name).click(function(){
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		$("#desktopMenu").removeClass("active");
		$("#desktop-map").removeClass("active");
		$("#topNav-desktop").removeClass("active");
		$(".logo").addClass("hidden-md");
		$(".logo-dark").removeClass("hidden-md");
		
		hideImpressum(true);
		
		$(".ajaxContent")
			.show('slide', { direction: "rigth" }, 500)
			.addClass('active')
			.css("overflow","scroll")
			.css("-webkit-overflow-scrolling" ,"touch")
			.load("assets/"+name+".html");
		
		
		
		
		$('.desktop.menuButton').addClass('thirdlevel-menu').removeClass("open");
		isScroll= true;
		setTimeout(function() {
			
			$('#desktopMenu').hide('slide', { direction: "rigth" },500).addClass("thirdlevel-menu");
			$('#ajaxContentDesktop').scrollTop(0);
			setTimeout(function(){
				isScroll=false;
			}, 300);
		}, 100);
		
		
		removeMenuActive();
		
		$('#desktopMenu').addClass(''+name+'');
		$('#ajaxContentDesktop').addClass(''+name+'');
		
		
		
		setTimeout(function() {
			
			contactScroll(name);				
			impressumEvent();
			
			setTimeout(function() {
				
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2Desktop(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
			
			}, 100);
		}, 100);
		
	});
}
	 
function pageEvents2(name){
		
						
	$("."+name+".another-page").click(function(){
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		
		$("#body").fadeOut(300);

		 
		setTimeout(function() {
			hideImpressum(false);
			$(".ajaxContent")
				.load("assets/"+name+".html");
			$("#body")
				.fadeIn(300);
			
			
		},300);
		
				
		$('.mobile.menuButton').addClass('thirdlevel').addClass('from-another').removeClass('open');
		isScroll=true;
		setTimeout(function(){
			impressumEvent();
			contactEvents(name);
			contactScroll(name);
			
			$('.ajaxContent').scrollTop(0);
			
			
			setTimeout(function() {
				isScroll=false;	
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
				
			
			}, 100); 
		}, 400);
							
	});				
}
	
	


function pageEvents2Desktop(name){
	
	$(".logo").addClass("hidden-md");
	$(".logo-dark").removeClass("hidden-md");
	
	$("."+name+".another-page").click(function(){
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		removeMenuActive();
		
		
		
		$('#desktopMenu').addClass(''+name+'');
		$('#ajaxContentDesktop').addClass(''+name+'');
		$('.desktop.menuButton').addClass('thirdlevel').addClass('from-another').removeClass('open');
		$("body").fadeOut(300);
		
		
		setTimeout(function() {
			hideImpressum(false);
		},300);
		
		setTimeout(function() {
			
			$(".ajaxContent")
				.load("assets/"+name+".html")
				.addClass("active")
				.show(0)
				.css("overflow","scroll")
				.css("-webkit-overflow-scrolling" ,"touch")
				.css("display","block");
			
			contactScroll(name);	
			
			
			$("body").fadeIn(300);
			isScroll=true;
			$('#ajaxContentDesktop').scrollTop(0);
			$('.unitMenu').hide('slide', { direction: "rigth" },300);
			setTimeout(function() {
				isScroll=false;
				impressumEvent();
				$(".unitMenu")
					.css("display", "table");
				
				
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2Desktop(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}

			}, 400);
		},350);
		
			
	});
						
}

function gotoPageEvents(name){
	//window.history.pushState(null, name, "#"+name);
	PushState("reloadPage", "#"+name, "#"+name);
	//location.hash=name;
	
	$('#desktopMenu').addClass("thirdlevel-menu");
	$('#desktopMenu').addClass(''+name+'');
	
	    	
	$('.logo').addClass('hidden-xs').addClass("hidden-md");

	$(".ajaxContent").hide(0);
	
	$(".ajaxContent")   		
		.css("overflow","scroll")
		.css("-webkit-overflow-scrolling" ,"touch")
		.load("assets/"+name+".html")
		.show('slide', { direction: "rigth" }, 300)
		.addClass('active');
	
	$('.mobile.menuButton').addClass('thirdlevel');

	setTimeout(function() {
		impressumEvent();
		contactEvents(name);
		contactScroll(name);
	
		setTimeout(function() {
			
				
			for(i=0;i<pages.length;i++){
				if(name!=pages[i]){
					pageEvents2(pages[i]);
				}
				else{
					$("."+pages[i]+".another-page").unbind();
				}
			}
			
		}, 200);
	}, 200);
}

function gotoPage(name){

    $("#mobileMenu").addClass("thirdlevel-menu");
	$("."+name+"-goto").click(function(){
    	gotoPageEvents(name);
    });
    $(".welcome"+name).click(function(){
    	gotoPageEvents(name);
    });
	
}

function gotoPageEventsDesktop(name){
	removeMenuActive();
	
	$('#desktopMenu').addClass("thirdlevel-menu");
	$('#desktopMenu').addClass(''+name+'');
	
	$('#ajaxContentDesktop').addClass(''+name+'');
    	
    	
	$('.logo').addClass('hidden-xs').addClass("hidden-md");
	$("#logo-dark").removeClass("hidden-md");
	
	if($('.ajaxContentContact').is('.active')){
		
		$('.ajaxContentContact').removeClass("active").hide(0);
		
    	$('.unitMenu').removeClass("active");
    	$('.desktop-backTopmenu').removeClass("active");
    	
    	$("#desktopMenu").hide('slide', { direction: "rigth" }, 300);
	}
	else if($('.unitMenu').is('.active')){
		$("#desktopMenu").hide('slide', { direction: "rigth" }, 300);
		
	}
	else{
    	$("#desktopMenu").hide('slide', { direction: "rigth" }, 300);
    	
    	
		
	}
	$("#topNav-desktop").removeClass("active");
	$(".ajaxContent").show('slide', { direction: "rigth" }, 300)
	                     .toggleClass('active')
	                     .css("overflow","scroll")
	                     .css("-webkit-overflow-scrolling" ,"touch")
	                     .load("assets/"+name+".html"); 

	$("#desktopMenu").addClass("thirdlevel-menu");
	$('.desktop.menuButton').addClass('thirdlevel').removeClass("open");
	$(".desktop-contact").removeClass("active");

 

	setTimeout(function() {
		
		impressumEvent();
		
		contactEventsDesktop(name);
		contactScroll(name);
		
		setTimeout(function() {
			
				
			for(i=0;i<pages.length;i++){
				if(name!=pages[i]){
					pageEvents2Desktop(pages[i]);
				}
				else{
					$("."+pages[i]+".another-page").unbind();
				}
			}
				
			
		}, 200);
	}, 200);

}

function gotoPageDesktop(name){

    
    
	$("."+name+"-goto-desktop").click(function(){
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		gotoPageEventsDesktop(name);
	});
	
	
	$(".welcome"+name).click(function(){
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		gotoPageEventsDesktop(name);
    });
	
}




$.fn.openUnit = function () {
		
};

var visited= localStorage.getItem("visited");


visited=false; // for testing the start page

$(document).ready(function() {

	// NOTE(ruben): START INIT HISTORY

	if(bSupportsHistory)history.replaceState({"callback_type":"reloadPage", "data":document.location.href}, "",document.location.href);

	// NOTE(ruben): END INIT HISOTRY


	if(visited=="true"){
		$('#fullpage').fullpage({		
			anchors: ['Communication-page', 'Picture', 'Solution', 'About' ],
			sectionsColor: ['#990033', '#1BBC9B', '#7E8F7C'],
			continuousVertical: true, 
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['communication', 'picture', 'solution', 'about']

		});
	}
	else{
		$('#fullpage').fullpage({		
			anchors: ['1','2', '3', '4', '5' ],
			sectionsColor: ['#636262','#990033', '#1BBC9B', '#7E8F7C'],
			continuousVertical: true, 
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['willkommen','communication', 'picture', 'solution', 'about']

		});
	}
	var isTouchDevice = function() {  return 'ontouchstart' in window || 'onmsgesturechange' in window; };
	var isDesktop = window.screenX != 0 && !isTouchDevice() ? true : false;
	
	if ($(window).width() > 960) {
		isDesktop = true;
	}
	
	for($i=0;$i<pages.length;$i++){
		
		
		pageEvents(pages[$i]);
		
		
		pageEventsDesktop(pages[$i]);
		
		
		if(isDesktop!=true){
			gotoPage(pages[$i]);
		}
		else{
			gotoPageDesktop(pages[$i]);
		}
		
		
	}
	
	var hash = location.hash;
	
	
	var isTouchDevice = function() {  return 'ontouchstart' in window || 'onmsgesturechange' in window; };
	var isDesktop = window.screenX != 0 && !isTouchDevice() ? true : false;
		
	if ($(window).width() > 960) {
		isDesktop = true;
	}
	else if($(window).width() < 960){
		isDesktop = false;
	}
	
	if(hash=="#menu"){
		
		$(".logo").addClass("hidden-xs"); 
		if(isDesktop){
			$('#desktopMenu').show(0); 
			$("#topNav-desktop").toggleClass("active");
		}
		else{
			$('#mobileMenu').show(0); 
			$("#topNav-mobile").toggleClass("active");
		}
	}
	else if(hash=="#impressum"){
		$(".menu").addClass("thirdlevel-menu");
		$(".logo").addClass("hidden-xs");
		removeMenuActive();
		var name= "impressum";
		$(".ajaxUnitcontent")
			.show(0)
			.addClass('active')
			.load("assets/"+name+".html");
		
		
		setTimeout(function() {
			
			contactScroll(name);				
			
			contactEvents(name);
			
			setTimeout(function() {
			
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2Desktop(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}

			}, 100);
		}, 100);
	}
	else if(hash=="#map"){
		if(isDesktop){
			removeMenuActive();
			$("#desktop-map").addClass("active");
			$("#topNav-desktop").removeClass("active");
			
			$(this).addClass('active');
			
			$('.desktop.menuButton').removeClass("open");
	
			
			$(".desktop-backTopmenu").removeClass("active");
			
			$('.backBottommenu').addClass('active');
			$('.desktop.menuButton').addClass('third');
			
			$(".logo").addClass("hidden-md");
			
			$("#desktopMenu").addClass("thirdlevel-menu");
			$("#desktopMenu").hide(0);
			
			
			
			$(".ajaxContent")
						 .show(0)
			             .addClass('active')
			             .load("assets/map-desktop.html");	
			
			
			
			setTimeout(function(){
				$(".logo-wps").click(function(){
					window.location.href = '/';
				});
			},100);
		}
		else{
			$("#mobileMenu").addClass("thirdlevel-menu");
			$("#mobile-map").addClass('active');
			
			
			$("#topNav-mobile").removeClass("active");
			
			$('.mobile.menuButton').removeClass("open").addClass('thirdlevel');
			$("#mobileMenu").show(0);
			
			$(".ajaxContentContact")
					 .show(0)
	                 .addClass('active')
	                 .addClass('map')
	                 .load("assets/map-mobile.html");	
			$('.backTopmenu').addClass('active');
			$('.backBottommenu').addClass('active');
			$('.mobile.menuButton').addClass('third');
			
			setTimeout(function(){
				contactScroll();
				$(".wps-logo-back").click(function(){
					window.location.href = '/';
				});
			},100);
		}
		
	}
	
	else if(hash=="#thankyou"){
		removeMenuActive();	
		$('.logo').addClass('hidden-xs').addClass("hidden-md");
		$("#desktopMenu").addClass("thirdlevel-menu");
		$("#topNav-desktop").removeClass("active");
		
		$(".ajaxContent").show(0)
        	.toggleClass('active')
			.load("assets/contactconfirm.html");
		
		$('.desktop.menuButton').addClass('thirdlevel').removeClass("open");
		$(".desktop-contact").removeClass("active");
	}
	
	else if(hash != "#1" && hash != "#2" && hash != "#3" && hash != "#4" && hash != "#5"){
		hash= hash.replace('#','');
		
		var name= "";
		for(var i=0; i<pages.length;i++){
			if(hash==pages[i]){
				name= hash;
			}
		}
		
		if(name!=""){
			
				
			if(isDesktop){
				
				removeMenuActive();
		
				
			    	
			    	
		    	$('.logo').addClass('hidden-xs');
		    	
		    	
			    $("#desktopMenu").hide(0);
			    	
			   
		    	$("#topNav-desktop").removeClass("active");
		    	$(".ajaxContent").show(0)
	                 .toggleClass('active')
	                 .css("overflow","scroll")
	                 .css("-webkit-overflow-scrolling" ,"touch")
	                 .load("assets/"+name+".html"); 
				$("#desktopMenu").addClass("thirdlevel-menu");
				$('.desktop.menuButton').addClass('thirdlevel').removeClass("open");
				$(".desktop-contact").removeClass("active");
				
				
				
				setTimeout(function() {
					
					impressumEvent();
					
					contactEventsDesktop(name);
					contactScroll(name);
					
					setTimeout(function() {
						
							
						for(i=0;i<pages.length;i++){
							if(name!=pages[i]){
								pageEvents2Desktop(pages[i]);
								
							}
							else{
								$("."+pages[i]+".another-page").unbind();
							}
						}
							
						
					}, 100);
				}, 100);
				
				$('#desktopMenu').addClass(name);
				$('#ajaxContentDesktop').addClass(''+name+'');
				
			}
			else{
				$('.logo').toggleClass('hidden-xs');
				$(".ajaxContent").hide(0);
				
				$(".ajaxContent")   		
					.css("overflow","scroll")
					.css("-webkit-overflow-scrolling" ,"touch")
					.load("assets/"+name+".html")
					.show(0) 
					.addClass('active');
				
				$('.mobile.menuButton').addClass('thirdlevel');
				
				
				
			
			
			
				setTimeout(function() {
					impressumEvent();
					contactEvents(name);
					contactScroll(name);
					
					setTimeout(function() {
							
						for(i=0;i<pages.length;i++){
							if(name!=pages[i]){
								pageEvents2(pages[i]);
							}
							else{
								$("."+pages[i]+".another-page").unbind();
							}
						}
							
						
					}, 100);
				}, 100);
			}
			
		}
	}
	
	
	
	$('.desktop.menuButton').click(function(){
		
		$(this).toggleClass("open");
		
		
		
		if($(".ajaxContentContact").is(".active")){
			$(".ajaxContentContact")
				.toggle('slide', {direction : "right"},500)
				.removeClass("active");
			$(".desktop-backTopmenu").removeClass("active"); 
			$(".desktop-contact").removeClass("active"); 
		}
		else{
			if(!$(this).is(".open")){
				$("#desktopMenu").removeClass("active");
			}
			$('#desktopMenu')
				.toggle('slide', { direction: "right" },500);
			
			setTimeout(function(){
				$("#topNav-desktop")
					.toggleClass("active");
			}, 250);
		}
		
		if($(this).is(".open")){
			//location.hash="menu"; //menu hash
			
			setTimeout(function(){
				$("#desktopMenu").addClass("active");
			},500);
		}

	});
	
	
	$('.mobile.menuButton').click(function(){ 
		localStorage.setItem("thirdLevelPage", "");
		$(this).toggleClass("open");
		$(".contact").removeClass("active");
		
		if($(".ajaxContentContact").is(".active")){
			
			if($(".mobile.menuButton").is(".fourth")){
		    	$(".mobile.menuButton").removeClass("open");
		    	$(".mobile.menuButton").removeClass("fourth");
			}
			else{
		    	$(".mobile.menuButton").addClass("open");
		    	
		    	setTimeout(function(){
					$("#topNav-mobile")
						.toggleClass("active");
				}, 250);
			}		 
		 
			$(".backBottommenu").removeClass("active");
			
			$(".contact-base").removeClass("active");
			$(".location-base").removeClass("active");
			$(".ajaxContentContact").hide('slide', {direction: "right"},500).removeClass("active");	; 
			
			
		}
		
		else if($(".ajaxContent").is(".active")){
			$(".mobile-backTopmenu").removeClass("active");  
			$(".mobile-contact").removeClass("active");  
			$('#mobileMenu')
				.toggle('slide', { direction: "down" },500);  
			$(".logo-dark").addClass("hidden-xs");	
			$(".logo").addClass("hidden-xs");
				
			setTimeout(function(){
				$("#topNav-mobile")
					.toggleClass("active");
			}, 250);
			
			
		}
		else{ 
			$('#mobileMenu')
				.toggle('slide', { direction: "down" },500); 
			$(".logo").toggleClass("hidden-xs"); 
			
			setTimeout(function(){
				$("#topNav-mobile")
					.toggleClass("active");
			}, 250);
		}

	});
	
	
	var isTouchDevice = function() {  return 'ontouchstart' in window || 'onmsgesturechange' in window; };
	var isDesktop = window.screenX != 0 && !isTouchDevice() ? true : false;
	
	if ($(window).width() > 960) {
		isDesktop = true;
	}
	

	
	//mobile contact
	$(".contact-base").click(function(){
		
		localStorage.setItem("thirdLevelPage", "");
		
		$(this).toggleClass('active');
		
		$("#topNav-mobile").removeClass("active");
		
		
		$('.mobile.menuButton').toggleClass("open").addClass('thirdlevel');
		

		$(".ajaxContentContact").toggle('slide', { direction: "rigth" }, 300)
		                 .toggleClass('active')
		                 .load("assets/contactform.html");	
		$('.backTopmenu').addClass('active');
		$('.backBottommenu').addClass('active');
		$('.mobile.menuButton').addClass('third');
		
		localStorage.setItem("thirdLevelPage", "");
		
		setTimeout(function() {
			logoBackToHome('wps-logo-to-home');
		}, 100);
		
	});
	
	
	
	$(".desktop-contact").click(function(){
		var tempWidth= document.getElementById("desktopMenu").style.width;
		
		$('.desktop.menuButton').removeClass("open");
      
		$('.desktop-backTopmenu').toggleClass('active').css("display","block");
		$('.backBottommenu').addClass('active');
		$('.desktop.menuButton').addClass('third').addClass("contact");
		
		$(".ajaxContent").removeClass("active");
		$(".ajaxContentContact")
			.toggle('slide', { direction: "rigth" }, 500)
			.addClass('active')
			.load("assets/contactform.html");
	
		$(".desktop-contact").toggleClass("active");
		
	});
	
	 
	
	$("#mobile-map").click(function(){
		var name="map";
		PushState("reloadPage", "#"+name, "#"+name);
		
		$(this).addClass('active');
		
		$("#topNav-mobile").removeClass("active");
		
		$('.mobile.menuButton').removeClass("open").addClass('thirdlevel');
		

		$(".ajaxContentContact").toggle('slide', { direction: "rigth" }, 300)
		                 .addClass('active')
		                 .addClass('map')
		                 .load("assets/map-mobile.html");	
		$('.backTopmenu').addClass('active');
		$('.backBottommenu').addClass('active');
		$('.mobile.menuButton').addClass('third');
		
		setTimeout(function(){
			$(".wps-logo-back").click(function(){
				window.location.href = '/';
			});
		},100);
		 
	});
	
	$("#desktop-map").click(function(){
		var name="map";
		//window.history.pushState(null, name, "#"+name);
		PushState("reloadPage", "#"+name, "#"+name);
		//location.hash=name;
		
		removeMenuActive();
		$("#desktopMenu").removeClass("active");
		$("#topNav-desktop").removeClass("active");
		
		$(this).addClass('active');
		$('.desktop.menuButton').removeClass("open");

		
		$(".desktop-backTopmenu").removeClass("active");
		
		$('.backBottommenu').addClass('active');
		$('.desktop.menuButton').addClass('third');
		 
		$(".logo").addClass("hidden-md");
		
		$(".ajaxContent") 
			 .show('slide', { direction: "rigth" }, 500)
             .addClass('active')
             .load("assets/map-desktop.html");	
		
		setTimeout(function() {
			$('#desktopMenu').hide('slide', { direction: "rigth" },500).addClass("thirdlevel-menu");
			if($(".ajaxContentContact").is(".active")){
				$(".ajaxContentContact").hide('slide', { direction: "rigth" },500).removeClass("active");
				$(".desktop-contact").removeClass("active");
			}
		}, 100);
		
		setTimeout(function(){
			$(".logo-wps").click(function(){
				window.location.href = '/';
			});
		},100);
		
	});

	
	$(".backTopmenu").click(function(){
		$(this).removeClass('active');
		$(".ajaxContent").removeClass('active');
		$(".contact").toggleClass('active');
		$('.unitMenu').show();
		
		localStorage.setItem("thirdLevelPage", "");
		
	});
	
	$(".desktop-backTopmenu").click(function(){  
		$(this).removeClass('active');
		
	
		var tempWidth= document.getElementById("desktopMenu").style.width;
		
		$(".desktop-contact").removeClass("active");
		$(".ajaxContent").removeClass('active');
		$(".desktop-contact").removeClass('active');
		$('.desktop.menuButton.third').removeClass("thirdlevel");
		$('.desktop.menuButton.third').removeClass("third");
		$(".desktop.menuButton").addClass("open");
		$(".desktop.menuButton.fourth").removeClass("open");
		if($(".desktop.menuButton").hasClass("fourth")){
			$(".ajaxContent").addClass('active');
			$('.ajaxContentContact').hide('slide', { direction: "rigth" },500).removeClass('active');
			$(".desktop.menuButton").removeClass("fourth");
		}
		else if($(".desktop.menuButton").hasClass("contact")){
			$('.ajaxContentContact').hide('slide', { direction: "rigth" },500).removeClass('active');
			$('.unitMenu').show();
			$(".desktop.menuButton").removeClass("contact");
		}
		else{
			if(tempWidth=="100%"){
				setTimeout(function(){
					$('.ajaxContent').hide('slide', { direction: "rigth" },500).removeClass('active');
					$('.unitMenu').show();
				}, 300);
			}
			else{
				$('.ajaxContent').hide('slide', { direction: "rigth" },500).removeClass('active');
				$('.unitMenu').show();
			}
			document.getElementById("topNav-desktop").style.display = "table";
		}
	});		
	
	
	$(".wps-logo-back").click(function(){
		$('.mobile.menuButton').removeClass("open");
		$('.mobile-menu').toggle('slide', { direction: "down" },500);
		$('.logo').removeClass('hidden-xs');
		
		localStorage.setItem("thirdLevelPage", "");
	});

	
	$(".logo-wps").click(function(){
		window.location.href = '/';
	});
    
    $(".backBottommenu").click(function(){
    	$(this).removeClass("active");
    	$(".contact").removeClass("active");
    	
    	if($(".mobile.menuButton").is(".fourth")){
	    	$(".mobile.menuButton").removeClass("open");
	    	$(".mobile.menuButton").removeClass("fourth");
    	}
    	else{
	    	$(".mobile.menuButton").addClass("open");
	    	
	    	setTimeout(function(){
				$("#topNav-mobile")
					.toggleClass("active");
			}, 250);
    	}
    	
    	$(".contact-base").removeClass("active");
    	$(".location-base").removeClass("active");
		$(".ajaxContentContact").hide('slide', {direction: "right"},500).removeClass("active");
		
		localStorage.setItem("thirdLevelPage", "");
		
    });
    
     window.addEventListener('popstate', function(event){
		 if(window.location.hash) {
            var hash = window.location.hash;
            
            console.log(hash + " - " );
		}
	});
	
});


function PushState(type, data, url)
{
	if(bSupportsHistory)
	{
		var state = {"callback_type":type, "data":data};
		window.history.pushState(state, "", url);
	}
}

window.onpopstate = function (oEvent)
{
	if(oEvent.state)
	{
		var state = oEvent.state;
		
		if(state.callback_type == "reloadPage")
		{
			document.location = state.data;
			document.location.reload();
		}
	}
};



