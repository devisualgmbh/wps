var pages=[
	"communication",
	"picture",
	"solution",
	"about",
];


function logoBackToHome(className){
	$("."+className).click(function(){
		$('.mobile.menuButton').removeClass("open");
		$('.mobile-menu').toggle('slide', { direction: "right" },500);
		
		if($('.ajaxContent').is('.active')){
			$('.ajaxContent').hide('slide', { direction: "right" },500).removeClass('active');
		}
		if($('.ajaxUnitcontent').is('.active')){
			$('.ajaxUnitcontent').hide('slide', { direction: "right" },500).removeClass('active');
		}
		
		$(".ajaxContent").removeClass('active');  
		$(".logo").removeClass('hidden-xs');
		$(".mobile.menuButton").removeClass('thirdlevel').removeClass('third');
		$(".backBottommenu").removeClass("active");
		//$(".unitMenu").removeClass("active");
		$('.unitMenu').show();
		$('.contact-base').removeClass("active");
	});
}


function contactEvents(name){

	$("#contact-"+name).click(function(){
		$(this).toggleClass('active');
		console.log('Contact Clicked');
		$('.unitMenu').toggle();
		$(".ajaxContent").toggle('slide', { direction: "rigth" }, 300)
		                 .toggleClass('active')
		                 .load("assets/contactform.html");
		$('.backTopmenu').toggleClass('active');
		$('.backBottommenu').toggleClass('active');
		//$('.menuButton').toggleClass('open');
		$('.menuButton').toggleClass('fourth');
		
		setTimeout(function() {
			logoBackToHome('wps-logo-to-home');
		
		
			$/*(".sendButton").click(function(){
				$(".ajaxContent").load("assets/contactconfirm.html");
				$('.backBottommenu').removeClass('active');
				$('.mobile.menuButton').toggleClass('open').addClass('thirdlevel');
	    	});*/
    	}, 100);
		 
	});
	
}

function pageEvents(name){
	$("."+name).click(function(){
		
		$(".ajaxUnitcontent").show('slide', { direction: "rigth" }, 300)
		                     .toggleClass('active')
		                     .load("assets/"+name+".html");
		
		$('.mobile.menuButton').toggleClass("open").addClass('thirdlevel');
		
		setTimeout(function() {
			contactEvents(name);
			
			setTimeout(function() {
				console.log("another page load");
				
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
				
			
			}, 100);
		}, 100);
		
	});
}
	
function pageEvents2(name){
		
						
	$("."+name+".another-page").click(function(){
		
		$(".unitMenu")
			.css("display", "none");
		$(".topNav")
			.css("display", "none");
		
		
		$(".ajaxUnitcontent")
			//.show('slide', { direction: "rigth" }, 300)
			//.addClass('active')
			.fadeOut(300);
			//.fadeIn(600)
			
		
		setTimeout(function() {
			
			$(".ajaxUnitcontent")
				.load("assets/"+name+".html")
				.fadeIn(300);
			
			setTimeout(function() {
				$(".unitMenu")
					.css("display", "table");
				$(".topNav")
					.css("display", "table");
			}, 300);
		},300);
		
				
		$('.mobile.menuButton').addClass('thirdlevel').addClass('from-another').removeClass('open');
		//$('.ajaxUnitcontent').scrollTop(0);
		
		
		
		setTimeout(function() {
			contactEvents(name);
				
			setTimeout(function() {
				console.log("another page load");
				
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
				
			
			}, 300);
		}, 300);
							
	});
					
		//console.log("page event created");
		//console.log("pages[$i] -> "+name);						
}
	
	
	//contactEvents(name);



function gotoPage(name){

    	
	$("."+name+"-goto").click(function(){
    	$('.logo').toggleClass('hidden-xs');
    	$('.mobile-menu').toggle('slide', { direction: "right" },300);
    	$(".ajaxUnitcontent").toggleClass('active')
			.load("assets/"+name+".html")
			.toggle('slide', { direction: "rigth" }, 300);
		
		$('.mobile.menuButton').addClass('thirdlevel');
		
	
	
	
		setTimeout(function() {
			contactEvents(name);
			console.log(name);
			setTimeout(function() {
				console.log("another page load");
					
				for(i=0;i<pages.length;i++){
					if(name!=pages[i]){
						pageEvents2(pages[i]);
					}
					else{
						$("."+pages[i]+".another-page").unbind();
					}
				}
					
				
			}, 200);
		}, 200);
    });
	
}


$.fn.openUnit = function () {
		
};

var visited= localStorage.getItem("visited");


$(document).ready(function() {
	
	if(visited=="true"){
		$('#fullpage').fullpage({		
			anchors: ['Communication', 'Picture', 'Solution', 'About' ],
			sectionsColor: ['#990033', '#1BBC9B', '#7E8F7C'],
			continuousVertical: true, 
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['Communication', 'Picture', 'Solution', 'About']

		});
	}
	else{
		$('#fullpage').fullpage({		
			anchors: ['Willkommen','Communication', 'Picture', 'Solution', 'About' ],
			sectionsColor: ['#636262','#990033', '#1BBC9B', '#7E8F7C'],
			continuousVertical: true, 
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['Willkommen','Communication', 'Picture', 'Solution', 'About']

		});
	}
	
	$('.desktop.menuButton').click(function(){
		$(this).toggleClass("open");	
		$('.backTopmenu').removeClass('active');
		$('.desktop-menu').toggle('slide', { direction: "rigth" }, 500);
		$('.desktop.menuButton.from-another').toggleClass('from-another');
		
		if($(this).is('.thirdlevel')){
				console.log('Has class thirdlevel');
				if($('.ajaxContent').is('.active')){
					$('.ajaxContent').hide('slide', { direction: "rigth" },500).removeClass('active');
				}
				if($('.ajaxUnitcontent').is('.active')){
					$('.ajaxUnitcontent').hide('slide', { direction: "rigth" },500).removeClass('active');
				}
				if($('.unitMenu').is('.active')){
					$('.unitMenu').show();
				}
				$(this).removeClass('thirdlevel');
				$('.logo').addClass('hidden-xs');
				if($('.contact').is('.active')){
					$('.contact').removeClass('active');
				}
		} else {
				console.log('Has no class Confirm');
				//$('.desktop-menu').toggle('slide', { direction: "rigth" },500);
		}
	});
	
	
	
	$('.mobile.menuButton').click(function(){
		$(this).toggleClass("open");
		$('.logo').toggleClass('hidden-xs');
		$('.backBottommenu').removeClass('active');
		$('.unitMenu').addClass('active');
		
		$('.mobile.menuButton.from-another').toggleClass('from-another');
		
		if($(this).is('.thirdlevel')){
				console.log('Has class thirdlevel');
				if($('.ajaxContent').is('.active')){
					$('.ajaxContent').hide('slide', { direction: "rigth" },500).removeClass('active');
				}
				if($('.ajaxUnitcontent').is('.active')){
					$('.ajaxUnitcontent').hide('slide', { direction: "rigth" },500).removeClass('active');
				}
				if($('.unitMenu').is('.active')){
					$('.unitMenu').show();
				}
				$(this).removeClass('thirdlevel');
				$('.logo').addClass('hidden-xs');
				if($('.contact').is('.active')){
					$('.contact').removeClass('active');
				}
		} else {
				console.log('Has no class Confirm');
				$('.mobile-menu').toggle('slide', { direction: "down" },500);
		}
		 

	});
	
	
	
	
	for($i=0;$i<pages.length;$i++){
		gotoPage(pages[$i]);
		pageEvents(pages[$i]);
		
	}


	
	
	$(".contact-base").click(function(){
		$(this).toggleClass('active');
		console.log('Contact Clicked');
		$('.mobile.menuButton').toggleClass("open").addClass('thirdlevel');
		$('.unitMenu').toggle();
/* 				$(".ajaxContent").toggleClass('active').toggleClass('contact') *//* .show() *//* .load("assets/contactform.html") */;
		$(".ajaxContent").toggle('slide', { direction: "rigth" }, 300)
		                 .toggleClass('active')
		                 .load("assets/contactform.html");	
		$('.backTopmenu').toggleClass('active');
		$('.backBottommenu').toggleClass('active');
		$('.mobile.menuButton').toggleClass('third');
		
		setTimeout(function() {
			logoBackToHome('wps-logo-to-home');
		}, 100);
		
	});
	
	
	
	$(".location-base").click(function(){
		$(this).toggleClass('active');
		console.log('location Clicked');
		$('.mobile.menuButton').toggleClass("open").addClass('thirdlevel');
		$('.unitMenu').toggle();
/* 				$(".ajaxContent").toggleClass('active').toggleClass('contact') *//* .show() *//* .load("assets/contactform.html") */;
		$(".ajaxContent").toggle('slide', { direction: "rigth" }, 300)
		                 .toggleClass('active')
		                 .load("assets/map-kopie.html");	
		$('.backTopmenu').toggleClass('active');
		$('.backBottommenu').toggleClass('active');
		$('.mobile.menuButton').toggleClass('third');
	});
/*	
	$(".location").click(function(){
		$(this).toggleClass('active');
		$(".ajaxContent").toggle('slide', { direction: "rigth" }, 300).toggleClass('active').load("assets/map.html");
	});
*/
	
	$(".backTopmenu").click(function(){
		$(this).removeClass('active');
		$(".ajaxContent").removeClass('active');
		$(".contact").toggleClass('active');
		$('.unitMenu').show();
	});
		
	$(".backBottommenu").click(function(){
		$(this).removeClass('active');
		$(".ajaxContent").toggle('slide', { direction: "rigth" }, 300).delay(800).removeClass('active');
		$(".contact").removeClass('active');
		$(".location").removeClass('active');
		$('.unitMenu').show();
		$('.mobile.menuButton.third').toggleClass("open");
		$('.mobile.menuButton.third').toggleClass("thirdlevel");
		$('.mobile.menuButton.third').toggleClass('third');
		$('.mobile.menuButton.fourth').addClass("thirdlevel");
		$('.mobile.menuButton.fourth').removeClass("open");
		$('.mobile.menuButton.fourth').toggleClass("fourth");
	});
	
	$(".wps-logo-back").click(function(){
		$('.mobile.menuButton').removeClass("open");
		$('.mobile-menu').toggle('slide', { direction: "down" },500);
		$('.logo').removeClass('hidden-xs');
			
	});
	
	
});